<?php

class Tovar
{

    const SHOW_BY_DEFAULT = 6;

    public static function getLatestTovars($count = self::SHOW_BY_DEFAULT)
    {
        $db = Db::getConnection();

        $sql = 'SELECT id, name, price, is_new, is_recommended, availability FROM tovar '
                . 'WHERE status = "1" ORDER BY id DESC '
                . 'LIMIT :count';

        $result = $db->prepare($sql);
        $result->bindParam(':count', $count, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        
        $result->execute();

        $i = 0;
        $tovarsList = array();
        while ($row = $result->fetch()) {
            $tovarsList[$i]['id'] = $row['id'];
            $tovarsList[$i]['name'] = $row['name'];
            $tovarsList[$i]['price'] = $row['price'];
            $tovarsList[$i]['is_new'] = $row['is_new'];
            $tovarsList[$i]['is_recommended']= $row['is_recommended'];
            $tovarsList[$i]['availability']= $row['availability'];
            $i++;
        }
        return $tovarsList;
    }

    public static function getTovarsListByCategory($categoryId, $page = 1)
    {
        $limit = Tovar::SHOW_BY_DEFAULT;
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
        $db = Db::getConnection();

        $sql = 'SELECT id, name, price, is_new, is_recommended, availability FROM tovar '
                . 'WHERE status = 1 AND category_id = :category_id '
                . 'ORDER BY id ASC LIMIT :limit OFFSET :offset';

        $result = $db->prepare($sql);
        $result->bindParam(':category_id', $categoryId, PDO::PARAM_INT);
        $result->bindParam(':limit', $limit, PDO::PARAM_INT);
        $result->bindParam(':offset', $offset, PDO::PARAM_INT);

        $result->execute();

        $i = 0;
        $tovars = array();
        while ($row = $result->fetch()) {
            $tovars[$i]['id'] = $row['id'];
            $tovars[$i]['name'] = $row['name'];
            $tovars[$i]['price'] = $row['price'];
            $tovars[$i]['is_new'] = $row['is_new'];
            $tovars[$i]['is_recommended']= $row['is_recommended'];
            $tovars[$i]['availability']= $row['availability'];
            $i++;
        }
        return $tovars;
    }

    public static function getTovarById($id)
    {
        $db = Db::getConnection();

        $sql = 'SELECT * FROM tovar WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $result->execute();

        return $result->fetch();
    }

    public static function getTotalTovarsInCategory($categoryId)
    {
        $db = Db::getConnection();

        $sql = 'SELECT count(id) AS count FROM tovar WHERE status="1" AND category_id = :category_id';

        $result = $db->prepare($sql);
        $result->bindParam(':category_id', $categoryId, PDO::PARAM_INT);

        $result->execute();

        $row = $result->fetch();
        return $row['count'];
    }

    public static function getTovarsByIds($idsArray)
    {
        $db = Db::getConnection();

        $idsString = implode(',', $idsArray);

        $sql = "SELECT * FROM tovar WHERE status='1' AND id IN ($idsString)";

        $result = $db->query($sql);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $i = 0;
        $tovars = array();
        while ($row = $result->fetch()) {
            $tovars[$i]['id'] = $row['id'];
            $tovars[$i]['code'] = $row['code'];
            $tovars[$i]['name'] = $row['name'];
            $tovars[$i]['price'] = $row['price'];
            $tovars[$i]['is_recommended']= $row['is_recommended'];
            $tovars[$i]['availability']= $row['availability'];
            $i++;
        }
        return $tovars;
    }

    public static function getRecommendedTovars()
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT id, name, price, is_new, availability FROM tovar '
                . 'WHERE status = "1" AND is_recommended = "1" '
                . 'ORDER BY id DESC');
        $i = 0;
        $tovarsList = array();
        while ($row = $result->fetch()) {
            $tovarsList[$i]['id'] = $row['id'];
            $tovarsList[$i]['name'] = $row['name'];
            $tovarsList[$i]['price'] = $row['price'];
            $tovarsList[$i]['is_new'] = $row['is_new'];
            $tovarsList[$i]['availability']= $row['availability'];
            $i++;
        }
        return $tovarsList;
    }

    public static function getTovarsList()
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT id, name, price, code, availability FROM tovar ORDER BY id ASC');
        $tovarsList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $tovarsList[$i]['id'] = $row['id'];
            $tovarsList[$i]['name'] = $row['name'];
            $tovarsList[$i]['code'] = $row['code'];
            $tovarsList[$i]['price'] = $row['price'];
            $tovarsList[$i]['availability']= $row['availability'];
            $i++;
        }
        return $tovarsList;
    }

    public static function deleteTovarById($id)
    {
        $db = Db::getConnection();

        $sql = 'DELETE FROM tovar WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateTovarById($id, $options)
    {
        $db = Db::getConnection();

        $sql = "UPDATE tovar
            SET 
                name = :name, 
                code = :code, 
                price = :price, 
                category_id = :category_id, 
                brand = :brand, 
                availability = :availability, 
                description = :description, 
                is_new = :is_new, 
                is_recommended = :is_recommended, 
                status = :status
            WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':code', $options['code'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
        $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        return $result->execute();
    }

    public static function createTovar($options)
    {
        $db = Db::getConnection();

        $sql = 'INSERT INTO tovar '
                . '(name, code, price, category_id, brand, availability,'
                . 'description, is_new, is_recommended, status)'
                . 'VALUES '
                . '(:name, :code, :price, :category_id, :brand, :availability,'
                . ':description, :is_new, :is_recommended, :status)';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':code', $options['code'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
        $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        if ($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;
    }

    public static function getAvailabilityText($availability)
    {
        switch ($availability) {
            case '1':
                return 'В наявності';
                break;
            case '0':
                return 'Під замовлення';
                break;
        }
    }

    public static function getImage($id)
    {
        $noImage = 'no-image.jpg';

        $path = '/upload/images/tovars/';

        $pathToTovarImage = $path . $id . '.jpg';

        if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToTovarImage)) {
            return $pathToTovarImage;
        }

        return $path . $noImage;
    }

}
