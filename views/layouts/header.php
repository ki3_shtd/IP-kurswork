﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Главная</title>
        <link href="/maket/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">
            <div id="wrapper">
                <header>
                    <div class="account">
                        <?php
                        if(isset($_SESSION['user'])) {
                            echo '<a href = "/cabinet/" ><button > Обліковий запис </button ></a >
                    <a href = "/user/logout/" ><button > Вихід</button ></a >';
                        }
                        if(isset($_SESSION['user'])===false) {
                            echo '<a href="/user/register/"><button>Реєстрація</button></a>
                <a href="/user/login/"><button>Вхід</button></a>';
                        } ?>
                    </div>
                    <h1>Електроніка</h1>
                </header>
                <nav>
                    <ul class="top-menu">
                        <a href="/"><li>Головна</li></a>
                        <a href="/catalog/"><li>Рекомендації</li></a>
                        <a href="/cart/"><li>Замовлення</li></a>
                        <a href="/about/"><li>Про нас</li></a>
                        <a href="/contacts/"><li>Контакти</li></a>
                    </ul>
                </nav>
                <aside>
                    <nav>
                        <ul class="aside-menu">
                            <?php foreach ($categories as $categoryItem): ?>
                                <a href="/category/<?php echo $categoryItem['id']; ?>"><li>
                                        <?php echo $categoryItem['name']; ?></li>
                                    </a>
                            <?php endforeach; ?>
                        </ul>
                    </nav>
                </aside>