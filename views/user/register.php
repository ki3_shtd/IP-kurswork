<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <h2 class="hed">Реєстрація</h2>
            <div class="user">
                <?php if ($result): ?>
                    <p>Вы зареєстровані!</p>
                <?php else: ?>

                    <form action="#" method="post">
                        <table>
                            <tr><td><label>Ім'я</label></td><td><input type="text" name="name" value="<?php echo $name; ?>"/></td></tr>
                            <tr><td><label>E-mail</label></td><td><input type="email" name="email" value="<?php echo $email; ?>"/></td></tr>
                            <tr><td><label>Пароль</label></td><td><input type="password" name="password" value="<?php echo $password; ?>"/></td></tr>
                            <tr><td></td><td><input type="submit" name="submit" value="Реєстрація" /></td></tr>
                        </table>
                    </form>
                    <?php if (isset($errors) && is_array($errors)): ?>

                        <?php foreach ($errors as $error): ?>
                            <p class="erorr"> - <?php echo $error; ?></p>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>