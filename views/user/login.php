<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">

        <h2 class="hed">Вхід на сайт</h2>
        <div class="user">
            <form action="#" method="post">
                <table >
                    <tr><td><label>Логін</label></td><td><input type="email" name="email" value="<?php echo $email; ?>"/></td></tr>
                    <tr><td><label>Пароль</label></td><td><input type="password" name="password" value="<?php echo $password; ?>"/></td></tr>
                    <tr><td></td><td> <input type="submit" name="submit" value="Вхід" /></td></tr>
                </table>
            </form>
            <?php if (isset($errors) && is_array($errors)): ?>

                <?php foreach ($errors as $error): ?>
                    <p class="erorr"> - <?php echo $error; ?></p>
                <?php endforeach; ?>

            <?php endif; ?>
        </div>

    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>