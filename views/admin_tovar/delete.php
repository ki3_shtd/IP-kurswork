<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <h2 class="hed">Видалити товар #<?php echo $id; ?></h2>
        <a href="/admin/tovar/">Повернутися назад</a>
            <p>Ви дійсно хочете видалити цей товар?</p>
            <form method="post">
                <input type="submit" name="submit" value="Видалити" />
            </form>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>

