<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <h2 class="hed">Керування товарами</h2>
            <p><a href="/cabinet/">Повернутися до кабінету</a></p>
            <p><a href="/admin/tovar/create">Додати товар</a></p>
            <h4>Список товарів</h4>
            <table class="tovar">
                <tr>
                    <th>ID</th>
                    <th>Артикул</th>
                    <th>Назва товара</th>
                    <th>Ціна</th>
                    <th></th>
                    <th></th>
                </tr>
                <?php foreach ($tovarsList as $tovar): ?>

                    <tr>
                        <td><?php echo $tovar['id']; ?></td>
                        <td><?php echo $tovar['code']; ?></td>
                        <td><?php echo $tovar['name']; ?></td>
                        <td><?php echo $tovar['price']; ?></td>
                        <td><a href="/admin/tovar/update/<?php echo $tovar['id']; ?>"><img class="ico" src="/maket/edit.png"></a></td>
                        <td><a href="/admin/tovar/delete/<?php echo $tovar['id']; ?>"><img class="ico" src="/maket/delete.png"></a></td>
                    </tr>
                <?php endforeach; ?>
            </table>

    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>

