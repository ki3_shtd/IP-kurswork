<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <h2 class="hed">Редагування товару #<?php echo $id; ?></h2>
        <a href="/admin/tovar">Повернутися назад</a>
        <form action="#" method="post" enctype="multipart/form-data">
            <table>
                <tr><td><label>Назва товару</label></td><td><input type="text" name="name" placeholder="" value="<?php echo $tovar['name']; ?>"></td></tr>
                <tr><td><label>Артикул</label></td><td><input type="text" name="code" value="<?php echo $tovar['code']; ?>"></td></tr>
                <tr><td><label>Ціна, грн</label></td><td><input type="text" name="price" value="<?php echo $tovar['price']; ?>"></td></tr>
                <tr><td><label>Категорія</label></td><td>
                        <select name="category_id">
                            <?php if (is_array($categoriesList)): ?>
                                <?php foreach ($categoriesList as $category): ?>
                                    <option value="<?php echo $category['id']; ?>"
                                        <?php if ($tovar['category_id'] == $category['id']) echo ' selected="selected"'; ?>>
                                        <?php echo $category['name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </td></tr>
                <tr><td><label>Виробник</label></td><td><input type="text" name="brand" value="<?php echo $tovar['brand']; ?>"></td></tr>
                <tr>
                    <td><label>Наявність на складі</label></td><td>
                        <select name="availability">
                            <option value="1" <?php if ($tovar['availability'] == 1) echo ' selected="selected"'; ?>>Так</option>
                            <option value="0" <?php if ($tovar['availability'] == 0) echo ' selected="selected"'; ?>>Ні</option>
                        </select>
                    </td></tr>
                <tr><td><label>Новинка</label></td><td>
                        <select name="is_new">
                            <option value="1" <?php if ($tovar['is_new'] == 1) echo ' selected="selected"'; ?>>Так</option>
                            <option value="0" <?php if ($tovar['is_new'] == 0) echo ' selected="selected"'; ?>>Ні</option>
                        </select>
                    </td></tr>
                <tr><td><label>Рекомендація</label></td><td>
                        <select name="is_recommended">
                            <option value="1" <?php if ($tovar['is_recommended'] == 1) echo ' selected="selected"'; ?>>Так</option>
                            <option value="0" <?php if ($tovar['is_recommended'] == 0) echo ' selected="selected"'; ?>>Ні</option>
                        </select>
                    </td></tr>
                <tr><td><label>Видимість</label></td><td>
                        <select name="status">
                            <option value="1" <?php if ($tovar['status'] == 1) echo ' selected="selected"'; ?>>Відображається</option>
                            <option value="0" <?php if ($tovar['status'] == 0) echo ' selected="selected"'; ?>>Прихований</option>
                        </select>
                    </td></tr>
                <tr><td><label>Детальний опис</label></td><td>
                        <textarea name="description"><?php echo $tovar['description']; ?></textarea>
                    </td></tr>
                <tr><td><label>Зображення</label></td><td>
                        <input type="file" name="image" value="<?php echo $tovar['image']; ?>"><br>
                        <img src="<?php echo Tovar::getImage($tovar['id']); ?>"/>
                    </td></tr>

                <tr><td></td><td><input type="submit" name="submit" value="Зберегти"></td></tr>
            </table>
        </form>

    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>

