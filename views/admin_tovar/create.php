<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <h2 class="hed">Новий товар</h2>
        <a href="/admin/tovar">Повернутися назад</a>
            <?php if (isset($errors) && is_array($errors)): ?>
                <ul>
                    <?php foreach ($errors as $error): ?>
                        <p class="erorr"> - <?php echo $error; ?></p>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>


                    <form action="#" method="post">
                    <table>
                        <tr><td><label>Назва товару</label></td><td><input type="text" name="name"></td></tr>
                        <tr><td><label>Артикул</label></td><td><input type="text" name="code"></td></tr>
                        <tr><td><label>Ціна, грн</label></td><td><input type="text" name="price"></td></tr>
                        <tr><td><label>Категорія</label></td><td>
                                <select name="category_id">
                                    <?php if (is_array($categoriesList)): ?>
                                        <?php foreach ($categoriesList as $category): ?>
                                            <option value="<?php echo $category['id']; ?>">
                                                <?php echo $category['name']; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>

                            </td></tr>
                        <tr><td><label>Виробник</label></td><td><input type="text" name="brand"></td></tr>
                        <tr>
                            <td><label>Наявність на складі</label></td><td><select name="availability">
                                    <option value="1" selected="selected">Так</option>
                                    <option value="0">Ні</option>
                                </select>
                            </td></tr>
                        <tr><td><label>Новинка</label></td><td>
                                <select name="is_new">
                                    <option value="1" selected="selected">Так</option>
                                    <option value="0">Ні</option>
                                </select>
                            </td></tr>
                        <tr><td><label>Рекомендація</label></td><td>
                                <select name="is_recommended">
                                    <option value="1" selected="selected">Так</option>
                                    <option value="0">Ні</option>
                                </select>
                            </td></tr>
                        <tr><td><label>Видимість</label></td><td>
                                <select name="status">
                                    <option value="1" selected="selected">Відображаєтся</option>
                                    <option value="0">Прихований</option>
                                </select>
                            </td></tr>
                        <tr><td><label>Детальний опис</label></td><td>
                                <textarea name="description"></textarea>
                            </td></tr>
                        <tr><td><label>Зображення</label></td><td><input type="file" name="image"></td></tr>

                        <tr><td></td><td><input type="submit" name="submit" value="Зберегти"></td></tr>
                    </table>
                    </form>




    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>

