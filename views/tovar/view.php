<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-1-3">
                <img src="<?php echo Tovar::getImage($tovar['id']); ?>">
            </div>
            <div class="col-2-3">
                <h2><?php echo $tovar['name']; ?></h2>
                <h2>Ціна: <?php echo $tovar['price']; ?> гр</h2>
                <a href="/cart/add/<?php echo $tovar['id']; ?>"><button>Додати до кошику</button></a>
                <p>
                    <b>Наявність:</b>
                    <?php echo Tovar::getAvailabilityText($tovar['availability']); ?>
                </p>
                <p><b>Виробник:</b> <?php echo $tovar['brand']; ?></p>
                <p><b>Код товару: </b><?php echo $tovar['code']; ?></p>

                <?php if ($tovar['is_new']): ?>
                    <h3>Новинка!</h3>
                <?php endif; ?>
                <?php if ($tovar['is_recommended']): ?>
                    <h3>Рекомендовано</h3>
                <?php endif; ?>
                <p><b>Опис товару:</b></p>
                <?php echo $tovar['description']; ?>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>