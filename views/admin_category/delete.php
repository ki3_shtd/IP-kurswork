<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
            <h2 class="hed">Видалити категорію #<?php echo $id; ?></h2>
            <p><a href="/admin/category">Повернутися назад</a></p>
            <p>Ви дійсно хочете видалити цю категорію?</p>
            <form method="post">
                <input type="submit" name="submit" value="Видалити" />
            </form>

        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>

