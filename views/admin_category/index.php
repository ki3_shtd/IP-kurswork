<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <h2 class="hed">Керування категоріями</h2>
        <p><a href="/cabinet/">Повернутися до кабінету</a></p>
        <p><a href="/admin/category/create">Додати категорию</a></p>
        <h4>Список категорій</h4>
        <table class="tovar">
            <tr>
                    <th>ID</th>
                    <th>Назва категорії</th>
                    <th>Порядковий номер</th>
                    <th>Статус</th>
                    <th></th>
                    <th></th>
            </tr>
                <?php foreach ($categoriesList as $category): ?>
                    <tr>
                        <td><?php echo $category['id']; ?></td>
                        <td><?php echo $category['name']; ?></td>
                        <td><?php echo $category['sort_order']; ?></td>
                        <td><?php echo Category::getStatusText($category['status']); ?></td>
                        <td><a href="/admin/category/update/<?php echo $category['id']; ?>"><img class="ico" src="/maket/edit.png"></a></td>
                        <td><a href="/admin/category/delete/<?php echo $category['id']; ?>"><img class="ico" src="/maket/delete.png"></a></td>
                    </tr>
                <?php endforeach; ?>
            </table>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>

