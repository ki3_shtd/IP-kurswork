<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <h2 class="hed">Редагування категорії #<?php echo $id; ?></h2>
        <a href="/admin/category/">Повернутися назад</a>
        <form action="#" method="post">
            <table>
                <tr><td><label>Назва</label></td><td><input type="text" name="name" value="<?php echo $category['name']; ?>"></td></tr>
                <tr><td><label>Порядковий номер</label></td><td><input type="number" name="sort_order" value="<?php echo $category['sort_order']; ?>"></td></tr>
                <tr><td><label>Видимість</label></td><td>
                        <select name="status">
                            <option value="1" <?php if ($category['status'] == 1) echo ' selected="selected"'; ?>>Відображається</option>
                            <option value="0" <?php if ($category['status'] == 0) echo ' selected="selected"'; ?>>Прихована</option>
                        </select>
                    </td></tr>
                <tr><td><label> </label></td><td><input type="submit" name="submit" value="Зберегти"></td></tr>
            </table>
        </form>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>

