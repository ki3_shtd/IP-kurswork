<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <h2 class="hed">Додавання нової категорії</h2>
        <p><a href="/admin/category/">Повернутися назад</a></p>
            <?php if (isset($errors) && is_array($errors)): ?>
                    <?php foreach ($errors as $error): ?>
                        <p class="erorr"> - <?php echo $error; ?></p>
                    <?php endforeach; ?>
            <?php endif; ?>

            <form action="#" method="post">
                <table>
                    <tr><td><label>Назва</label></td><td><input type="text" name="name"></td></tr>
                    <tr><td><label>Порядковий номер</label></td><td><input type="number" name="sort_order"></td></tr>
                    <tr><td><label>Відображення</label></td><td>
                            <select name="status">
                                <option value="1" selected="selected">Відображається</option>
                                <option value="0">Прихована</option>
                            </select>
                        </td></tr>
                    <tr><td><label> </label></td><td><input type="submit" name="submit" value="Зберегти"></td></tr>
                </table>
            </form>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>

