<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <h2 class="hed">Рекомендовані товари</h2>
        <div class="row">
            <?php foreach ($recomendTovars as $tovar): ?>
                <div class="row">
                    <div class="col-1-3">
                        <img src="<?php echo Tovar::getImage($tovar['id']); ?>">
                    </div>
                    <div class="col-2-3">
                        <a href="/tovar/<?php echo $tovar['id']; ?>"><h2>
                                <?php echo $tovar['name']; ?></h2>
                        </a>
                        <h2>Ціна: <?php echo $tovar['price']; ?> грн</h2>

                        <h3>
                            Наявність:
                            <?php echo Tovar::getAvailabilityText($tovar['availability']); ?>
                        </h3>


                        <?php if ($tovar['is_new']): ?>
                            <h3>Новинка!</h3>
                        <?php endif; ?>
                        <?php if ($tovar['is_recommended']): ?>
                            <h3>Рекомендовано</h3>
                        <?php endif; ?>


                    </div>
                </div>
            <?php endforeach; ?>


                    



        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>