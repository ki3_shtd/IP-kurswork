<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">

        <h2 class="hed">Перегляд замовлення #<?php echo $order['id']; ?></h2>
        <p><a href="/admin/order/">Повернутися до списку замовлень</a></p>
        <p><a href="/admin/order/update/<?php echo $order['id']; ?>">Редагувати замовлення</a></p>


        <h3>Інформація про замовлення</h3>
            <table class="tovar">
                <tr>
                    <th>Номер замовлення</th>
                    <th>Им'я клієнта</th>
                    <th>Телефон кліента</th>
                    <?php if ($order['user_id'] != 0): ?>
                    <th>ID клієнта</th>
                    <?php endif; ?>
                    <th>Статус замовлення</th>
                    <th>Дата замовлення</th>
                </tr>
                <tr>
                    <td><?php echo $order['id']; ?></td>
                    <td><?php echo $order['user_name']; ?></td>
                    <td><?php echo $order['user_phone']; ?></td>
                    <?php if ($order['user_id'] != 0): ?>
                        <td><?php echo $order['user_id']; ?></td>
                    <?php endif; ?>
                    <td><?php echo Order::getStatusText($order['status']); ?></td>
                    <td><?php echo $order['date']; ?></td>
                </tr>


            </table>
            <h3>Товари в замовленні</h3>
            <table class="tovar">
                <tr>
                    <th>ID товару</th>
                    <th>Артикул товару</th>
                    <th>Назва</th>
                    <th>Ціна</th>
                    <th>Кількість</th>
                </tr>
                <?php foreach ($tovars as $tovar): ?>
                    <tr>
                        <td><?php echo $tovar['id']; ?></td>
                        <td><?php echo $tovar['code']; ?></td>
                        <td><?php echo $tovar['name']; ?></td>
                        <td><?php echo $tovar['price']; ?></td>
                        <td><?php echo $tovarsQuantity[$tovar['id']]; ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr class="total">
                    <th colspan="4">Загальна вартість, грн:</th>
                    <th><?php echo $totalPriceOrder;?></th>
                </tr>
            </table>



        </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>

