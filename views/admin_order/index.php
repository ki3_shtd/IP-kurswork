<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <h2 class="hed">Керування замовленнями</h2>
        <p><a href="/cabinet/">Повернутися до кабінету</a></p>
            <h4>Список замовлень</h4>
            <table class="tovar">
                <tr>
                    <th>ID</th>
                    <th>Им'я клієнта</th>
                    <th>Телефон</th>
                    <th>Дата оформлення</th>
                    <th>Статус</th>
                    <th></th>
                    <th></th>
                </tr>
                <?php foreach ($ordersList as $order): ?>
                    <tr>
                        <td>
                            <a href="/admin/order/view/<?php echo $order['id']; ?>">
                                <?php echo $order['id']; ?>
                            </a>
                        </td>
                        <td><?php echo $order['user_name']; ?></td>
                        <td><?php echo $order['user_phone']; ?></td>
                        <td><?php echo $order['date']; ?></td>
                        <td><?php echo Order::getStatusText($order['status']); ?></td>
                        <td><a href="/admin/order/update/<?php echo $order['id']; ?>"><img class="ico" src="/maket/edit.png"></a></td>
                        <td><a href="/admin/order/delete/<?php echo $order['id']; ?>"><img class="ico" src="/maket/delete.png"></a></td>
                    </tr>
                <?php endforeach; ?>
            </table>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>

