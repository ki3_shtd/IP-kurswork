<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <h2 class="hed">Замовлення</h2>
                <?php if ($tovarsInCart): ?>
                    <h2 class="hed">Ви обрали такі товари:</h2>
                    <table class="tovar">
                        <tr>
                            <th>Код товару</th>
                            <th>Назва</th>
                            <th>Вартість, грн</th>
                            <th>Кількість, шт</th>
                            <th>Видалити</th>
                        </tr>
                        <?php foreach ($tovars as $tovar): ?>
                            <tr>
                                <td><?php echo $tovar['code'];?></td>
                                <td>
                                    <a href="/tovar/<?php echo $tovar['id'];?>">
                                        <?php echo $tovar['name'];?>
                                    </a>
                                </td>
                                <td><?php echo $tovar['price'];?></td>
                                <td><?php echo $tovarsInCart[$tovar['id']];?></td>
                                <td>
                                    <a href="/cart/delete/<?php echo $tovar['id'];?>">
                                        Видалити
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <tr class="total">
                            <th colspan="4">Загальна вартість, грн:</th>
                            <th><?php echo $totalPrice;?></th>
                        </tr>

                    </table>
                    <a href="/cart/checkout"><button>Оформити замовлення</button></a>
                <?php else: ?>
                    <p>Корзина пуста</p>

                    <a href="/">На головну</a>
                <?php endif; ?>

                </div>






</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>