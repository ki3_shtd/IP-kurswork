<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <h2 class="hed">Замовлення</h2>
        <div class="row">
            <?php if ($result): ?>
                <p>Замовлення прийнято на обробку.</p>
            <?php else: ?>
                <p>Обрано <?php echo $totalQuantity; ?> товарів на сумму <?php echo $totalPrice; ?> грн</p>
                <p>Для оформления замовлення заповніть анкету.</p>
                    <?php if (!$result): ?>
                                <div class="user">
                                     <form action="#" method="post">
                                         <table>
                                             <tr><td><label>Ваше ім'я</label></td><td><input type="text" name="userName"  value="<?php echo $userName; ?>"/></td></tr>
                                             <tr><td><label>Телефон</label></td><td><input type="text" name="userPhone" value="<?php echo $userPhone; ?>"/></td></tr>
                                             <tr><td></td><td><label>+38**********</label></td></tr>
                                             <tr><td></td><td> <input type="submit" name="submit" value="Відправити замовлення" /></td></tr>
                                         </table>
                                     </form>
                                </div>
                                <?php if (isset($errors) && is_array($errors)): ?>
                                    <?php foreach ($errors as $error): ?>
                                        <p class="erorr"> - <?php echo $error; ?></p>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>