<?php include ROOT . '/views/layouts/header.php'; ?>

    <section>
        <div class="container">
            <h2 class="hed">Кабінет користувача <?php echo $user['name'];?></h2>
                    <p><a href="/cabinet/edit">Редагувати особисті дані</a></p>
                    <?php
                    if($user['role'] == 'admin') {
                        echo '<p><a href="/admin/tovar">Управління товарами</a></p>
                              <p><a href="/admin/category">Управління категоріями</a></p>
                              <p><a href="/admin/order">Управління замовленнями</a></p>';
                    } ?>


        </div>
    </section>

<?php include ROOT . '/views/layouts/footer.php'; ?>