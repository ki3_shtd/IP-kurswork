<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <h2 class="hed">Редагування даних</h2>
        <div class="user">
            <p><a href="/cabinet/">Повернутися до кабінету</a></p>
                        <?php if ($result): ?>
                            <p>Дані успішно змінено</p>
                        <?php else: ?>
                        <?php if (isset($errors) && is_array($errors)): ?>
                                <?php foreach ($errors as $error): ?>
                                    <p class="erorr"> - <?php echo $error; ?></p>
                                <?php endforeach; ?>
                        <?php endif; ?>
                        <form action="#" method="post">

                            <table>
                                <tr>
                                    <td><label>Ім'я</label></td>
                                    <td><input type="text" name="name" value="<?php echo $name; ?>"/></td>
                                </tr>
                                <tr>
                                    <td><label>Пароль</label></td>
                                    <td><input type="password" name="password" placeholder="Пароль" value="<?php echo $password; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td><td><input type="submit" name="submit" value="Зберегти дані"/></td>
                                </tr>
                            </table>
                        </form>
                <?php endif; ?>

    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>