<?php

class TovarController
{


    public function actionView($tovarId)
    {

        $categories = Category::getCategoriesList();

        $tovar = Tovar::getTovarById($tovarId);

        require_once(ROOT . '/views/tovar/view.php');
        return true;
    }

}
