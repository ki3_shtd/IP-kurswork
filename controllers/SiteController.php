<?php

class SiteController
{


    public function actionIndex()
    {

        $categories = Category::getCategoriesList();

        $latestTovars = Tovar::getLatestTovars(6);

        require_once(ROOT . '/views/site/index.php');
        return true;
    }

    public function actionContact()
    {
        $categories = Category::getCategoriesList();

        require_once(ROOT . '/views/site/contact.php');
        return true;
    }

    public function actionAbout()
    {
        $categories = Category::getCategoriesList();
        require_once(ROOT . '/views/site/about.php');
        return true;
    }

}
