<?php

class CartController
{

    public function actionAdd($id)
    {

        Cart::addTovar($id);


        $referrer = $_SERVER['HTTP_REFERER'];
        header("Location: $referrer");

    }
    

    public function actionDelete($id)
    {

        Cart::deleteTovar($id);

        header("Location: /cart");
    }

    public function actionIndex()
    {

        $categories = Category::getCategoriesList();


        $tovarsInCart = Cart::getTovars();

        if ($tovarsInCart) {

            $tovarsIds = array_keys($tovarsInCart);
            $tovars = Tovar::getTovarsByIds($tovarsIds);
            $totalPrice = Cart::getTotalPrice($tovars);
        }

        require_once(ROOT . '/views/cart/index.php');
        return true;
    }

    public function actionCheckout()
    {
        $tovarsInCart = Cart::getTovars();

        if ($tovarsInCart == false) {
            header("Location: /");
        }

        $categories = Category::getCategoriesList();

        $tovarsIds = array_keys($tovarsInCart);
        $tovars = Tovar::getTovarsByIds($tovarsIds);
        $totalPrice = Cart::getTotalPrice($tovars);

        $totalQuantity = Cart::countItems();

        $userName = false;
        $userPhone = false;

        $result = false;

        if (!User::isGuest()) {
            $userId = User::checkLogged();
            $user = User::getUserById($userId);
            $userName = $user['name'];
        } else {
            $userId = false;
        }


        if (isset($_POST['submit'])) {

            $userName = $_POST['userName'];
            $userPhone = $_POST['userPhone'];

            $errors = false;

            if (!User::checkName($userName)) {
                $errors[] = "Неправильне им'я";
            }
            if (!User::checkPhone($userPhone)) {
                $errors[] = "Неправильний телефон";
            }


            if ($errors == false) {

                $result = Order::save($userName, $userPhone, $userId, $tovarsInCart);

                if ($result) {
                    Cart::clear();
                }
            }
        }

        require_once(ROOT . '/views/cart/checkout.php');
        return true;
    }

}
