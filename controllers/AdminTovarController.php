<?php

class AdminTovarController extends AdminBase
{

    public function actionIndex()
    {

        self::checkAdmin();

        $tovarsList = Tovar::getTovarsList();

        $categories = Category::getCategoriesList();
        require_once(ROOT . '/views/admin_tovar/index.php');
        return true;
    }

    public function actionCreate()
    {

        self::checkAdmin();
        $categoriesList = Category::getCategoriesListAdmin();

        if (isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['code'] = $_POST['code'];
            $options['price'] = $_POST['price'];
            $options['category_id'] = $_POST['category_id'];
            $options['brand'] = $_POST['brand'];
            $options['availability'] = $_POST['availability'];
            $options['description'] = nl2br($_POST['description']);
            $options['is_new'] = $_POST['is_new'];
            $options['is_recommended'] = $_POST['is_recommended'];
            $options['status'] = $_POST['status'];

            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заполните поля';
            }

            if ($errors == false) {
                $id = Tovar::createTovar($options);


                if ($id) {

                    if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                        move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/upload/images/tovars/{$id}.jpg");
                    }
                };

                header("Location: /admin/tovar");
            }
        }

        $categories = Category::getCategoriesList();
        require_once(ROOT . '/views/admin_tovar/create.php');
        return true;
    }

    public function actionUpdate($id)
    {

        self::checkAdmin();

        $categoriesList = Category::getCategoriesListAdmin();

        $tovar = Tovar::getTovarById($id);


        if (isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['code'] = $_POST['code'];
            $options['price'] = $_POST['price'];
            $options['category_id'] = $_POST['category_id'];
            $options['brand'] = $_POST['brand'];
            $options['availability'] = $_POST['availability'];

            $options['description'] = nl2br($_POST['description']);

            $options['is_new'] = $_POST['is_new'];
            $options['is_recommended'] = $_POST['is_recommended'];
            $options['status'] = $_POST['status'];


            if (Tovar::updateTovarById($id, $options)) {

                if (is_uploaded_file($_FILES["image"]["tmp_name"])) {


                   move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/upload/images/tovars/{$id}.jpg");
                }
            }

            header("Location: /admin/tovar");
        }


        $categories = Category::getCategoriesList();
        require_once(ROOT . '/views/admin_tovar/update.php');
        return true;
    }

    /**
     * Action для страницы "Удалить товар"
     */
    public function actionDelete($id)
    {
        // Проверка доступа
        self::checkAdmin();

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена
            // Удаляем товар
            Tovar::deleteTovarById($id);
            // Перенаправляем пользователя на страницу управлениями товарами
            header("Location: /admin/tovar");
        }

        // Подключаем вид
        $categories = Category::getCategoriesList();
        require_once(ROOT . '/views/admin_tovar/delete.php');
        return true;
    }

}
