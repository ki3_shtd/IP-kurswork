<?php

class AdminOrderController extends AdminBase
{

    public function actionIndex()
    {

        self::checkAdmin();

        $ordersList = Order::getOrdersList();

        $categories = Category::getCategoriesList();
        require_once(ROOT . '/views/admin_order/index.php');
        return true;
    }

    public function actionUpdate($id)
    {

        self::checkAdmin();


        $order = Order::getOrderById($id);

        if (isset($_POST['submit'])) {

            $status = $_POST['status'];


            Order::updateOrderById($id, $status);


            header("Location: /admin/order/view/$id");
        }


        $categories = Category::getCategoriesList();
        require_once(ROOT . '/views/admin_order/update.php');
        return true;
    }

    public function actionView($id)
    {


        self::checkAdmin();
        $order = Order::getOrderById($id);
        $tovarsQuantity = json_decode($order['tovars'], true);
        $tovarsIds = array_keys($tovarsQuantity);
        $tovars = Tovar::getTovarsByIds($tovarsIds);

        $totalPriceOrder = 0;

        foreach ($tovars as $item) {
                $totalPriceOrder += $item['price'] * $tovarsQuantity[$item['id']];
            }

        $categories = Category::getCategoriesList();
        require_once(ROOT . '/views/admin_order/view.php');
        return true;
    }

    public function actionDelete($id)
    {

        self::checkAdmin();


        if (isset($_POST['submit'])) {
            Order::deleteOrderById($id);
            header("Location: /admin/order");
        }

        $categories = Category::getCategoriesList();
        require_once(ROOT . '/views/admin_order/delete.php');
        return true;
    }




}
