<?php

class Cart
{

    public static function addTovar($id)
    {
        $id = intval($id);
        $tovarsInCart = array();

        if (isset($_SESSION['tovars'])) {

            $tovarsInCart = $_SESSION['tovars'];
        }

        if (array_key_exists($id, $tovarsInCart)) {

            $tovarsInCart[$id] ++;
        } else {

            $tovarsInCart[$id] = 1;
        }
        $_SESSION['tovars'] = $tovarsInCart;
        return self::countItems();
    }

    public static function countItems()
    {

        if (isset($_SESSION['tovars'])) {

            $count = 0;
            foreach ($_SESSION['tovars'] as $id => $quantity) {
                $count = $count + $quantity;
            }
            return $count;
        } else {
            return 0;
        }
    }


    public static function getTovars()
    {
        if (isset($_SESSION['tovars'])) {
            return $_SESSION['tovars'];
        }
        return false;
    }


    public static function getTotalPrice($tovars)
    {

        $tovarsInCart = self::getTovars();


        $total = 0;
        if ($tovarsInCart) {

            foreach ($tovars as $item) {
                $total += $item['price'] * $tovarsInCart[$item['id']];
            }
        }

        return $total;
    }


    public static function clear()
    {
        if (isset($_SESSION['tovars'])) {
            unset($_SESSION['tovars']);
        }
    }


    public static function deleteTovar($id)
    {
        $tovarsInCart = self::getTovars();
        unset($tovarsInCart[$id]);
        $_SESSION['tovars'] = $tovarsInCart;
    }

}
